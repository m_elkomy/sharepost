<?php

    /*
     * bootstrap file
     * bootstrap all framework
     * include core class
     * include base controller
     * include database class
     * include the configuration file
     */
    require_once('Config/config.php');
    //load helpers
    require_once('Helpers/url_helper.php');
    require_once('Helpers/session_helper.php');
    //autoload the libraries classes
    spl_autoload_register(function($classname){
        require_once('libraries/'.$classname.'.php');
    })
//    require_once('libraries/Core.php');
//    require_once('libraries/Controller.php');
//    require_once('libraries/Database.php');


?>