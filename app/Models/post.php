<?php
class post{
    private $db;
    public function __construct()
    {
        $this->db = new Database;
    }

    public function getPosts(){
        $this->db->query('select *,
                                posts.id as PostId,
                                users.id as UserId,
                                posts.created_at as postCreated,
                                users.created_at as userCreated
                                from posts 
                                inner join users 
                                on posts.userid = users.id
                                order By posts.created_at desc');
        $result = $this->db->resultSet();

        return $result;

    }

    public function addPost($data){
        $this->db->query('insert into posts(title,userid,body) values(:title,:userid,:body)');
        $this->db->bind(':title',$data['title']);
        $this->db->bind(':userid',$data['user_id']);
        $this->db->bind(':body',$data['body']);
        if($this->db->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function getPostById($id){
        $this->db->query('select * from posts where id=:id');
        $this->db->bind(':id',$id);
        $row = $this->db->single();

        return $row;
    }

    public function updatePost($data){
        $this->db->query('update posts set title = :title,body=:body where id=:id');
        $this->db->bind(':title',$data['title']);
        $this->db->bind(':id',$data['id']);
        $this->db->bind(':body',$data['body']);
        if($this->db->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function deletePost($id){
        $this->db->query('delete from posts where id=:id');
        $this->db->bind(':id',$id);
        if($this->db->execute()){
            return true;
        }else{
            return false;
        }
    }
}