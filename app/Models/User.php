<?php
class User{
    private $db;
    public function __construct()
    {
        $this->db = new Database;
    }

    public function getUserByEmail($email){
        $this->db->query('select * from users where email=:email');
        $this->db->bind(':email',$email);
        $row = $this->db->single();

        //check for row
        if($this->db->rowCount()>0){
            return true;
        }else{
            return false;
        }
    }
    //add new user to db
    public function register($data){
        $this->db->query('insert into users(name,email,password) values(:name,:email,:password)');
        $this->db->bind(':name',$data['name']);
        $this->db->bind(':email',$data['email']);
        $this->db->bind(':password',$data['password']);
        if($this->db->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function login($email,$password){
        $this->db->query('select * from users where email=:email');
        $this->db->bind(':email',$email);

        $row = $this->db->single();

        $hashedPassword = $row->password;
        if(password_verify($password,$hashedPassword)){
            return $row;
        }else{
            return false;
        }
    }

    public function getUserById($id){
        $this->db->query('select * from users where id=:id');
        $this->db->bind(':id',$id);
        $row = $this->db->single();

        return $row;
    }
}