<?php
class Pages extends Controller {
    public function __construct()
    {
    }
    public function index(){
        if(isLoggedIn()){
            redirect('/posts');
        }
        $data = [
            'title'=>'SharePosts',
            'description'=>'Simple Social Network App Build On MVC PHP'
        ];
        $this->view('pages/index',$data);
    }
    public function about(){
        $data = [
            'title'=>'about page',
            'description'=>'App To Share Posts With Other User',
        ];
        $this->view('pages/about',$data);
    }
}