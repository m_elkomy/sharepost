<?php
class Users extends Controller {

    public function __construct()
    {
        $this->userModel = $this->model('user');
    }
    //register function
    public function register(){
        //check for post request
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //if there is post request proccess the form
            //sanitize form data
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
            //init data
            $data = [
                'name'=>trim($_POST['name']),
                'email'=>trim($_POST['email']),
                'password'=>trim($_POST['password']),
                'password_confirm'=>trim($_POST['password_confirm']),
                'name_error'=>'',
                'email_error'=>'',
                'password_error'=>'',
                'password_confirm_error'=>'',
            ];

            //validate the name
            if(empty($data['name'])){
                $data['name_error'] = 'Please Enter The Name';
            }elseif(strlen($data['name'])<6){
                $data['name_error'] = 'Name Must Be At Least 6 Chars';
            }
            //validate the email
            if(empty($data['email'])){
                $data['email_error'] = 'Please Enter The Email';
            }else{
                //check email
                if($this->userModel->getUserByEmail($data['email'])){
                    $data['email_error'] = 'Email Is Already Taken';
                }
            }

            //valide the password
            if(empty($data['password'])){
                $data['password_error'] = 'Please Enter The Password';
            }elseif(strlen($data['password'])<6){
                $data['password_error'] = 'Password Must Be At least 6 Chars';
            }

            if(empty($data['password_confirm'])){
                $data['password_confirm_error'] = 'Please Enter The Password Confirmation';
            }else{
                if(($data['password'] != $data['password_confirm'])){
                    $data['password_confirm_error'] = 'Password Confirm Not Match The Password';
                }
            }

            if(empty($data['name_error']) && empty($data['password_error']) && empty($data['email_error']) && empty($data['password_confirm_error'])){
                //proccess the data
                //hash the password
                $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                //REGISTER NEW USER
                if($this->userModel->register($data)){
                    flash('register_success','You Are Now Register Your Can Login Now');
                    redirect('/users/login');
                }else{
                    die('SomeThing Went Wrong');
                }
            }else{
                //load the view with errors
                $this->view('users/register',$data);
            }
        }else{
            //if there is no post request load the form
            //init array of data to hold form data
            $data = [
                'name'=>'',
                'email'=>'',
                'password'=>'',
                'password_confirm'=>'',
                'name_error'=>'',
                'email_error'=>'',
                'password_error'=>'',
                'password_confirm_error'=>'',
            ];

            //load the view
            $this->view('users/register',$data);
        }
    }

    public function login(){
        //check for post request
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //if there is post request proccess the form
            //sanitize form data
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
            //init data
            $data = [
                'email'=>trim($_POST['email']),
                'password'=>trim($_POST['password']),
                'email_error'=>'',
                'password_error'=>'',
                ];

            //validate the email
            if(empty($data['email'])){
                $data['email_error'] = 'Please Enter The Email';
            }

            //valide the password
            if(empty($data['password'])){
                $data['password_error'] = 'Please Enter The Password';
            }

            //check for user email
            if($this->userModel->getUserByEmail($data['email'])){
                //user found
            }else{
                //user not found
                $data['email_error'] = 'No User Found';
            }

            if(empty($data['password_error']) && empty($data['email_error'])){
                //proccess the data
                //check and set logged in user
                $loggedInUser = $this->userModel->login($data['email'],$data['password']);

                if($loggedInUser){
                    //session start
                    $this->createUserSession($loggedInUser);
                }else{
                    $data['password_error'] = 'Passwrod Incorrect';
                    $this->view('users/login',$data);
                }
            }else{
                //load the view with errors
                $this->view('users/login',$data);
            }
        }else{
            //if there is no post request load the form
            //init array of data to hold form data
            $data = [
                'email'=>'',
                'password'=>'',
                'email_error'=>'',
                'password_error'=>'',
            ];

            //load the view
            $this->view('users/login',$data);
        }
    }
    public function createUserSession($user){
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;
        redirect('/posts');
    }

    public function logout(){
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();
        return redirect('/users/login');
    }


}