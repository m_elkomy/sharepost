<?php
class Posts extends Controller {
    public function __construct()
    {
        if(!isLoggedIn()){
            redirect('/users/login');
        }

        $this->postModel = $this->model('post');
        $this->userModel = $this->model('user');
    }

    public function index(){
        //get posts
        $posts = $this->postModel->getPosts();
        $data = ['posts'=>$posts];
        $this->view('posts/index',$data);
    }

    public function add(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //sanitize the post
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
            $data = [
                'title'=>trim($_POST['title']),
                'body'=>trim($_POST['body']),
                'user_id'=>$_SESSION['user_id'],
                'title_error'=>'',
                'body_error'=>'',
            ];

            //validate the title
            if(empty($data['title'])){
                $data['title_error'] = 'You Must Enter The Title';
            }

            if(empty($data['body'])){
                $data['body_error']='You Must Enter The Body';
            }

            //make sure there is no error
            if(empty($data['title_error']) && empty($data['body_error'])){
                if($this->postModel->addPost($data)){
                    flash('Post_message','Post Added Successfully');
                    redirect('/posts');
                }else{
                    die('SomeThing Went Wrong');
                }
            }else{
                //load view with error
                $this->view('/posts/add',$data);
            }

        }else{

            $data = ['title'=>'','body'=>''];
            $this->view('posts/add',$data);
        }
    }

    public function show($id){
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($post->userid);
        $data = ['post'=>$post,'user'=>$user];
        $this->view('/posts/show',$data);
    }


    public function edit($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //sanitize the post
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
            $data = [
                'id'=>$id,
                'title'=>trim($_POST['title']),
                'body'=>trim($_POST['body']),
                'user_id'=>$_SESSION['user_id'],
                'title_error'=>'',
                'body_error'=>'',
            ];

            //validate the title
            if(empty($data['title'])){
                $data['title_error'] = 'You Must Enter The Title';
            }

            if(empty($data['body'])){
                $data['body_error']='You Must Enter The Body';
            }

            //make sure there is no error
            if(empty($data['title_error']) && empty($data['body_error'])){
                if($this->postModel->updatePost($data)){
                    flash('Post_message','Post Updated Successfully');
                    redirect('/posts');
                }else{
                    die('SomeThing Went Wrong');
                }
            }else{
                //load view with error
                $this->view('/posts/edit',$data);
            }

        }else{
            //check for owner
            $post = $this->postModel->getPostById($id);

            if($post->userid!=$_SESSION['user_id']){
                redirect('/posts');
            }
            $data = ['id'=>$id,'title'=>$post->title,'body'=>$post->body];
            $this->view('posts/edit',$data);
        }
    }

    public function delete($id){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $post = $this->postModel->getPostById($id);

            if($post->userid!=$_SESSION['user_id']){
                redirect('/posts');
            }
            if($this->postModel->deletePost($id)){
                flash('Post_message','Post Removed');
                redirect('/posts');
            }else{
                die('Something Went Wrong');
            }
        }else{
        redirect('/posts');
        }
    }
}