<?php

    /*
     * database class
     * connect to db
     * run query
     */
    class Database{
        private $host   = DB_HOST;
        private $user   = DB_USER;
        private $pass   = DB_PASS;
        private $dbname = DB_NAME;
        private $dbh;
        private $stmt;
        private $error;

        public function __construct()
        {
            //set dsn
            $dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname;
            $options = array(
                PDO::ATTR_PERSISTENT=>true,
                PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
            );
            try{
                $this->dbh = new PDO($dsn,$this->user,$this->pass,$options);
            }catch (PDOException $e){
                $this->error = $e->getMessage();
                echo $this->error;
            }
        }

        //prepared statment
        public function query($sql){
            $this->stmt = $this->dbh->prepare($sql);
        }
        //bind value
        public function bind($params,$values,$type=null){
            if(is_null($type)){
                switch (true){
                    case is_int($values):
                        $type = PDO::PARAM_INT;
                        break;
                    case is_bool($values):
                        $type = PDO::PARAM_BOOL;
                        break;
                    case is_null($values):
                        $type = PDO::PARAM_NULL;
                        break;
                    default:
                        $type = PDO::PARAM_STR;
                }
            }
            $this->stmt->bindvalue($params,$values,$type);
        }
        //execute the query
        public function execute(){
            return $this->stmt->execute();
        }
        //get result set
        public function resultSet(){
            $this->execute();
            return $this->stmt->fetchAll(PDO::FETCH_OBJ);
        }
        //get single row
        public function single(){
            $this->execute();
            return $this->stmt->fetch(PDO::FETCH_OBJ);
        }
        //get row count
        public function rowCount(){
            $this->execute();
            return $this->stmt->rowCount();
        }
    }