<?php

    /*
     * core class
     * get the url
     * handle the url
     * format the url to controller/method/params
     * load the requested controller
     */
    class Core{
        protected $currentController = 'Pages';
        protected $currentMethod     = 'index';
        protected $params            = [];



        public function __construct()
        {
            //call the geturl function
            $url = $this->getUrl();
            //looking for the first value in the url the controller
            if(file_exists('../app/Controllers/'.ucwords($url[0]).'.php')){
                //if the requested file exist set it to current controller
                $this->currentController = ucwords($url[0]);
                //unset the 0 index
                unset($url[0]);
            }
            //include the requested file
            require_once('../app/Controllers/'.$this->currentController.'.php');
            //instantiate the current controller
            $this->currentController = new $this->currentController;

            //looking for the seconed value the method
            if(isset($url[1])){
                if(method_exists($this->currentController,$url[1])){
                    //if method exist in controller set it to current method
                    $this->currentMethod = $url[1];
                    //unset the 1 index
                    unset($url[1]);
                }
            }

            //looking for the third value the params
            $this->params = $url ? array_values($url) : [];
            //call a callback function with array of params
            call_user_func_array([$this->currentController,$this->currentMethod],$this->params);
        }

        //function to get the url
        public function getUrl(){
            //check to see if url is exist
            if(isset($_GET['url'])){
                //trim it from forward slah
                $url = rtrim($_GET['url'],'/');
                $url = filter_var($url,FILTER_SANITIZE_URL);
                $url = explode('/',$url);
                return $url;
            }
        }
    }



?>