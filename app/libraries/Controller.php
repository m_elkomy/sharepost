<?php

    /*
     * base controller class
     * load the view for the requested controller
     * load the model for the requested controller
     */
    class Controller{
        //load the model
        public function model($model){
            if(file_exists('../app/Models/'.$model.'.php')){
                //if file exists require it
                require_once('../app/Models/'.$model.'.php');
                //instantiate the model
                return new $model;
            }
        }

        //load the view
        public function view($view,$data=[]){
            //check to see if view exists
            if(file_exists('../app/Views/'.$view.'.php')){
                require_once('../app/Views/'.$view.'.php');
            }else{
                die('Sorry This View Not Exists');
            }
        }
    }